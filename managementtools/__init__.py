# -*- coding: utf-8 -*-

"""Top-level package for Django Management Tools."""

__author__ = """Steve Graham"""
__email__ = 'stgraham2000@gmail.com'
__version__ = '0.2.2'
