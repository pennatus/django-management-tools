#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `django-management-tools` package."""


import unittest

from managementtools.management.commands import createadmin


class TestDjangoManagementTools(unittest.TestCase):
    """Tests for `django-management-tools` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_000_something(self):
        """Test something."""
